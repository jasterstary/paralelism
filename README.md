# Paralelism

A PHP library for running multiple time consuming commands in parallel without need to use multithreading.
Main function is waiting until all scripts are done, and then continues with their results.
Timeout can be set: if running time exceeds timeout, still running script instances are killed.

Typical usage:

```

    $P = new \App\Libraries\Paralelism\Paralelism();
    
    $P->add('cmd001', '/usr/local/bin/script1.sh 5');
    $P->add('cmd002', '/usr/local/bin/script1.sh 15');
    $P->add('cmd003', '/usr/local/bin/script2.sh 3');
    $P->add('cmd004', '/usr/local/bin/script3.sh 10');

    $P->setTimeout(20);

    $P->start();
    $P->wait();


```


It is possible to use callbacks, for example from controller:

```

  $P->setOnStart([$this, '_on_start'])->setOnWaiting([$this, '_on_waiting'])->setOnStop([$this, '_on_stop']);

```


Callback method will receive as a first parameter Paralelism class instance:

```
// controller class
...
  public function _on_waiting($P){
    $data = [
      'user' => $this->visitor->getUser(),
      'status' => $P->getStatus(),
      'elapsed' => $this->waxed->utils->showtime($P->getElapsedTime()),
    ];
    $this->waxed->title($data['status']);
    $this->waxed->pick('main .info')->display($data, '/test/info')->chunk();
  }
...


```

Running script could be stopped also from different web request.
For such functionality is needed to keep data about running processes either in database or session.
Paralelism library has methods for packing/unpacking essential data to string.

```
//... IN START METHOD:

    $P->start();
    $USER->storeData('paralelism', $P->pack());
    $P->wait();

//... IN STOP METHOD:

    $P = new \App\Libraries\Paralelism\Paralelism();
    $P->unpack($USER->retrieveData('paralelism'));
    $P->stop();

```

For more examples, check Test/Test.php controller.
