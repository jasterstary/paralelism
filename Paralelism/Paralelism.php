<?php namespace App\Libraries\Paralelism;

/**
 * A library for running multiple commands parallel in PHP.
 *
 * {@link https://gitlab.com/jasterstary/paralelism}
 *
 * Library allows to run multiple processes without need to use multithreading.
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @author Jaster Stary {@link https://gitlab.com/jasterstary}
 */


/**
 * Internal class maintaining one process.
 *
 */ 
class ParalelismProcess{

    private $pid;
    private $command;

    public function __construct($cl=false){
        if ($cl != false){
            $this->command = $cl;
            $this->runCom();
        }
    }

    private function runCom(){
        $command = 'nohup '.$this->command.' > /dev/null 2>&1 & echo $!';
        exec($command ,$op);
        $this->pid = intval($op[0]);
    }

    public function setPid($pid){
        $this->pid = $pid;
    }

    public function getPid(){
        return $this->pid;
    }

    public function status(){
      $command = 'ps -p '.$this->pid;
      exec($command,$op);
      if (!isset($op[1]))return false;
      else {
        return true;
      }
    }

    public function start(){
        if ($this->command != '')$this->runCom();
        else return true;
    }

    public function stop($sig = 9){ //9 SIGKILL
      if (!$this->status()) return true;
      posix_kill($this->pid, $sig);
      if (!$this->status()) {
        return true;
      } else {
        return false;
      };
    }
}

class Paralelism{
  
  private $_instances = [];
  private $_running = false;
  private $_timeout = false;
  private $_status = 'inited';

/**
 * Set timeout for maximum duration of running scripts.
 *
 * @param int $sec (seconds to timeout)
 * @param int $signal (kill signal. Default: 9 => SIGKILL)
 * @return this
 */ 
  function setTimeout($sec, $sig = 9) { // 9 SIGKILL
    $this->_timeout = intval($sec);
    $this->_timeoutsignal = $sig;
    return $this;
  }

/**
 * Adds one process.
 * Never ever allow user input here!
 * This could be called only before start method.
 *
 * @param string $name
 * @param string $cmd
 * @return boolean
 */ 
  function add($name, $cmd) {
    if ($this->_running) return false;
    $this->_instances[$name] = $cmd;
    return true;
  }


/**
 * returns actual status of operation.
 *
 * @return string
 */ 
  function getStatus() {
    return $this->_status;
  }

/**
 * returns elapsed time so far.
 *
 * @return int
 */ 
  function getElapsedTime() {
    return time() - $this->_running;
  }

/**
 * returns list of process ids.
 *
 * @return array
 */   
  function getPids() {
    $a = [];
    foreach ($this->_instances as $name => $obj) {
      //if ($obj->status()) 
      $a[$name] = $obj->getPid();
    }
    return $a;
  }

/**
 * returns serialized representation of essential data.
 *
 * @return string
 */ 
  public function pack(): string {
    return serialize([
      'started' => $this->_running,
      'pids' => $this->getPids(),
    ]);
  }

/**
 * restores the class from string presentation of essential data.
 *
 * @param string $data
 * @return boolean
 */ 
  public function unpack(string $data): void {
    $data = unserialize($data);
    if (!is_array($data['pids'])) return false;
    foreach ($data['pids'] as $name => $pid) {
      $this->_instances[$name] = new ParalelismProcess();
      $this->_instances[$name]->setPid($pid);
    }
    if (!isset($data['started'])) {
      $this->_running = time();
    } else {
      $this->_running = intval($data['started']);
    };
    return true;
  }

/**
 * Starts execution of scripts.
 *
 * @return this
 */ 
  function start() {
    if ($this->_running) return false;
    foreach ($this->_instances as $name => $cmd) {
      $this->_instances[$name] = new ParalelismProcess($cmd);
    }
    $this->_running = time();
    if (isset($this->_on_start)) {
      call_user_func($this->_on_start, $this);
    };
    return $this;
  }

/**
 * Stops execution of scripts using kill signal.
 *
 * @param int $signal (default: 9 => SIGKILL)
 * @return bool
 */ 
  function stop($sig = 9) {
    if (!$this->_running) return false;
    foreach ($this->_instances as $name => $obj) {
      $obj->stop($sig);
    }
    $i = 0;
    while(true){
      sleep(1);
      $a = false;
      foreach ($this->_instances as $obj) {
        if ($obj->status()) {
          $a = true;
          break;
        };
      };
      $now = time() - $this->_running;
      if (!$a) {
        break;
      };
      if (isset($this->_on_waiting)) {
        call_user_func($this->_on_waiting, $this);
      };
      $i++;
      if ($i > 10) {
        return false;
      };
    };
    return true;
  }

/**
 * Waiting for end of all scripts.
 *
 * @return this
 */ 
  function wait() {
    while(true){
      sleep(1);
      $a = false;
      foreach ($this->_instances as $obj) {
        if ($obj->status()) {
          $a = true;
          break;
        };
      };
      if (!$a) {
        break;
      };
      $this->_status = 'running';
      $now = time() - $this->_running;
      if (isset($this->_on_waiting)) {
        call_user_func($this->_on_waiting, $this);
      };
      if (($this->_timeout) && ($now >= $this->_timeout)) {
        $this->_status = 'timeout';
        if($this->stop($this->_timeoutsignal)){
          break;
        };
      };
    }
    $this->_running = false;
    if ($this->_status != 'timeout') $this->_status = 'done';
    if (isset($this->_on_stop)) {
      call_user_func($this->_on_stop, $this);
    };
    return $this;
  }

/**
 * Setting on-start callback.
 * Callback is called when all scripts started.
 * 
 * @param callable $method
 * @return this
 */ 
  function setOnStart($method) {
    $this->_on_start = $method;
    return $this;
  }

/**
 * Setting on-waiting callback.
 * Callback is called periodically during execution of scripts.
 * 
 * @param callable $method
 * @return this
 */ 
  function setOnWaiting($method) {
    $this->_on_waiting = $method;
    return $this;
  }

/**
 * Setting on-stop callback.
 * Callback is called when all scripts ended.
 * 
 * @param callable $method
 * @return this
 */ 
  function setOnStop($method) {
    $this->_on_stop = $method;
    return $this;
  }


}
