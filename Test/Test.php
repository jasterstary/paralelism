<?php namespace App\Controllers;

/***
 * (c) Jaster Stary 2021
 *
 */

class Test extends BaseController {

  //-------------------------------------------------------------------- CI4 CONSTRUCTOR-LIKE:
  public function initController(
    \CodeIgniter\HTTP\RequestInterface $request,
    \CodeIgniter\HTTP\ResponseInterface $response,
    \Psr\Log\LoggerInterface $logger) {
    parent::initController($request, $response, $logger);
    
    $a = explode('.', $_SERVER['HTTP_HOST']);
    $domain = implode('.', array_slice($a, -2));
    $subdomain = implode('.', array_slice($a, -3, 1));
    
    if (is_file(APPPATH . 'Config/Local/' . $domain . '/config.php')) {
      if (is_file(APPPATH . 'Config/Local/' . $domain . '/' . $subdomain . '/config.php')) {
        $config = require(APPPATH . 'Config/Local/' . $domain . '/' . $subdomain . '/config.php');
      } else {
        $config = require(APPPATH . 'Config/Local/' . $domain . '/config.php');
      };
    };
   
    $exceptions = new \App\Libraries\Exceptions\Exceptions();

    $this->waxed = new \App\Libraries\Waxed\Waxed();
    $this->waxed->setup($config['waxed']);

    $this->redbean = new \App\Libraries\Redbean\Redbean();
    $this->redbean->setup($config['redbean']);
  }

  //-------------------------------------------------------------------- SERVE HTML/CSS:
  public function design() {
    $this->response->removeHeader('Content-type');
    $this->waxed->design->dispatch(implode('/', func_get_args()));
  }

  //-------------------------------------------------------------------- SERVE JS PLUGINS:
  public function plugin() {
    $this->response->removeHeader('Content-type');
    $c = \Config\Services::cache();
    $this->waxed->plugin->setCache($c);
    $this->waxed->plugin->dispatch(implode('/', func_get_args()));
  }

  //-------------------------------------------------------------------- SERVE INDEX:
  public function index() {
    
    // we want following plugins on page:
    $this->waxed->plugin->uses(
      'base', 'jsonviewer', 'longpolling', 'bootstrap',
      'tingle', 'datatables', 'pagination'
    );
    
    // Page title:
    $this->waxed->title('Test !');

    // What we knew about visitor?:
    $this->visitor = new \App\Libraries\Visitor\Visitor($this);
    $this->visitor->start(new \App\Models\UserModel());
    
    $user = $this->visitor->getUser();
    
    // Draw the main box:
    $this->waxed->pick('main')->display([
      'user' => $user,
    ], '/test/hello');
    
    // Show flash messages, if any:
    $m = $this->visitor->getMessages();
    if ($m) {
      $this->waxed->pick('messages')->display([
        'messages' => $m,
      ], '/home/messages');      
    };
    
    // Draw the footer box:
    $this->waxed->pick('footer')->display([
      'user' => $user,
    ], '/test/footer');
    
    // Flush out the basic view:
    $this->waxed->view('main', '/test/index');
  }

  //-------------------------------------------------------------------- SERVE AJAX REQUESTS:
  public function action() {
    if (!isset($_POST['action']))$_POST['action'] = '';

    // Are there some authentication requests from visitor?:
    $this->visitor = new \App\Libraries\Visitor\Visitor($this);
    try {
      $this->visitor->start(new \App\Models\UserModel());
    } catch (\App\Libraries\Exceptions\InputException $e) {
      $this->waxed->pick($_POST['dialog'])->invalidate($e->getErrors())->flush(); 
      return;
    } catch (\App\Libraries\Exceptions\ReloadException $e) {
      $this->waxed->reload()->flush(); 
      return;
    }
    
    // Here we register all methods prefixed with "__" from this controller:
    $this->waxed->action->register($this, 'controller');
    
    // Lets do, what we can do. What we cant do, is exception:
    try {
      $this->waxed->action->dispatch($_POST);
    } catch (\App\Libraries\Exceptions\InputException $e) {
      $this->waxed->pick($_POST['dialog'])->invalidate($e->getErrors())->flush(); 
      return;
    } catch (\App\Libraries\Exceptions\ReloadException $e) {
      $this->waxed->reload()->flush(); 
      return;
    }      
    
    $this->waxed->flush();
  }
  


  //-------------------------------------------------------------------- [ callbacks ]

  public function _on_start($P){
    $data = [
      'user' => $this->visitor->getUser(),
      'status' => $P->getStatus(),
      'pids' => $P->getPids(),
      'elapsed' => 0,
    ];
    $this->waxed->title($data['status']);
    
    //$this->waxed->pick('main .info')->display($data, '/test/info')->chunk();
  }

  public function _on_stop($P) {
    $data = [
      'user' => $this->visitor->getUser(),
      'status' => $P->getStatus(),
    ];
    $a = \R::findOne( 'paralelism', 'user LIKE ? and id = ? and ended is null ORDER BY started DESC',
        [ $this->visitor->getUser()['tempid'], $P->ID ]
    );
    if($a) {
      $a->ended = time();
      if($a->killed) $data['status'] = 'killed';
      \R::store($a);      
    }
    $this->waxed->title($data['status']);
    $this->waxed->pick('main')->display($data, '/test/ondone')->chunk();
  }

  
  public function _on_waiting($P){
    $data = [
      'user' => $this->visitor->getUser(),
      'status' => $P->getStatus(),
      'elapsed' => $this->waxed->utils->showtime($P->getElapsedTime()),
    ];
    $this->waxed->title($data['status']);
    $this->waxed->pick('main .info')->display($data, '/test/info')->chunk();
  }

  //-------------------------------------------------------------------- APPLICATION AJAX REQUESTS:

  //-------------------------------------------------------------------- [ start ]
  public function __test_start($input){
    $P = new \App\Libraries\Paralelism\Paralelism();
    $P->add('cmd001', 'sleep 5');
    $P->add('cmd002', 'sleep 15');
    $P->add('cmd003', 'sleep 3');
    $P->add('cmd004', 'sleep 10');
    $P->setTimeout(20)->setOnStart([$this, '_on_start'])->setOnWaiting([$this, '_on_waiting'])->setOnStop([$this, '_on_stop']);
    $data = [
      'user' => $this->visitor->getUser(),
    ];
    
    $P->start();
    
    $pa = \R::dispense('paralelism');
    $pa->packed = $P->pack();
    $pa->user = $this->visitor->getUser()['tempid'];
    $pa->started = time();
    $P->ID = $data['id'] = \R::store($pa);    
    $this->waxed->pick('main')->display($data, '/test/onwaiting')->chunk();
    $P->wait();
    
  }

  //-------------------------------------------------------------------- [ stop ]
  public function __test_stop($input){
    $a = \R::findOne( 'paralelism', 'user LIKE ? AND id = ? and ended is null ORDER BY started DESC',
        [ $this->visitor->getUser()['tempid'], intval($input['id']) ]
    );
    if(!$a)die();
    $P = new \App\Libraries\Paralelism\Paralelism();
    $P->unpack($a->packed);
    $a->killed = time();
    \R::store($a);
    $P->stop();
  }

  //-------------------------------------------------------------------- [ default ]
  public function __default() {

  }

  //-------------------------------------------------------------------- AUTHENTICATION AJAX REQUESTS:

  //-------------------------------------------------------------------- [ dialog/login ]
  public function __dialog_login() {
    if (isset($_POST['dialog'])) {
      $this->waxed->pick($_POST['dialog'])->display([
        '_dialog_signature_' => $_POST['dialog'],
        'user' => $this->visitor->getUser(),

      ], '/home/login');
      
    } else {
      $this->waxed->pick('main')->dialog([
        'user' => $this->visitor->getUser(),
      ], '/home/login', 0, false, true );
    }
  }

  //-------------------------------------------------------------------- [ dialog/register ]
  public function __dialog_register() {
    $this->waxed->pick($_POST['dialog'])->display([
      '_dialog_signature_' => $_POST['dialog'],
      'user' => $this->visitor->getUser(),

    ], '/home/register');    
  }

  //-------------------------------------------------------------------- [ dialog/forgotten ]
  public function __dialog_forgotten() {
    $this->waxed->pick($_POST['dialog'])->display([
      '_dialog_signature_' => $_POST['dialog'],
      'user' => $this->visitor->getUser(),

    ], '/home/forgotten');
  }
  
  //-------------------------------------------------------------------- [ dialog/dismiss ]
  public function __dialog_dismiss() {
    $this->waxed->pick($_POST['dialog'])->dialogClose();
  }
  
  public function __show_cookie() {
    $this->waxed->pick('main')->dialog([

    ], '/home/aboutcookies', 0, false, false); 
  }
  
  //-------------------------------------------------------------------- [ login/sent ]
  public function __login_sent() {
    try {
      $this->USER_MODEL->login($_POST['login'], $_POST['password']);
    } catch (Exception $e) {
      $this->waxed->pick('dialog')->invalidate($e)->flush();
      exit;
    }
    $this->waxed->reload();
  }

}
